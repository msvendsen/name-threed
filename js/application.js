$(document).ready(function(){

	$('#main-form').validate({
		rules:{
			gender: "required",
			year: "required"
		},
		submitHandler: function(form){

			var gender = $('#gender').val();
			var year = $('#year').val();

			jQuery.ajax({
				type: "POST",
 				url: "http://ardbeg.thesven.com/popular-names/data-gatherer.php",
 				dataType: 'json',
  				data: {gender: gender, year: year }
			}).done(function(data){
				
				$('.hero-unit').hide();
				ThreeD.init(data);

			});

		}
	});
	
});

ThreeD = {
	variables: {
		rawData: null,
		frequencyTotal: 0,
		containerWidth: 0,
		containerHeight: 0,
		viewAngle: 45,
		aspect: 0,
		near: 0.1,
		far: 10000,
		container: null,
		renderer: null,
		camera: null,
		scene: null,
		light: null,
		names: null,
		dragControls: null
	},
	init: function(data){
		ThreeD.variables.rawData = data.values;
		if(ThreeD.variables.renderer != null){
			ThreeD.removeObjects();
		} else {
			ThreeD.variables.container = $('#three-js-container');
			ThreeD.variables.containerWidth = ThreeD.variables.container.width();
			ThreeD.variables.containerHeight = ThreeD.variables.container.height();
			ThreeD.variables.aspect = ThreeD.variables.containerWidth / ThreeD.variables.containerHeight;
			ThreeD.createScene(data);
		}
	},
	createScene: function(){
		ThreeD.variables.renderer = new THREE.WebGLRenderer();
		ThreeD.variables.camera = new THREE.PerspectiveCamera(ThreeD.variables.viewAngle, ThreeD.variables.aspect, ThreeD.variables.near, ThreeD.variables.far);
		ThreeD.variables.scene = new THREE.Scene();
		ThreeD.variables.scene.add(ThreeD.variables.camera);
		ThreeD.variables.camera.position.z = 1500;
		ThreeD.variables.renderer.setSize(ThreeD.variables.containerWidth, ThreeD.variables.containerHeight);
		ThreeD.variables.container.append(ThreeD.variables.renderer.domElement);
		
		//set up lights
		ThreeD.variables.light = new THREE.PointLight(0xFFFFFF);
		ThreeD.variables.light.position.x = 10;
		ThreeD.variables.light.position.y = 50;
		ThreeD.variables.light.position.z = 130;
		ThreeD.variables.scene.add(ThreeD.variables.light);
		
		ThreeD.variables.dragControls = new THREE.TrackballControls(ThreeD.variables.camera, ThreeD.variables.renderer.domElement);
		ThreeD.variables.dragControls.rotateSpeed = 1.0;
		ThreeD.variables.dragControls.zoomSpeed = 1.2;
		ThreeD.variables.dragControls.panSpeed = 0.8;
		ThreeD.variables.dragControls.noZoom = false;
		ThreeD.variables.dragControls.noPan = false;
		ThreeD.variables.dragControls.staticMoving = true;
		ThreeD.variables.dragControls.dynamicDampingFactor = 0.3;
		ThreeD.variables.dragControls.keys = [ 65, 83, 68 ];
		ThreeD.variables.dragControls.addEventListener('change', ThreeD.render);
		
		ThreeD.createObjects();
	},
	createObjects: function(){
		ThreeD.variables.names = [];
		var i = 0,
				max = ThreeD.variables.rawData.length;
		for(i; i<max; i++){
			ThreeD.variables.frequencyTotal += parseInt(ThreeD.variables.rawData[i].Frequency);
		}
		i = 0;
		for(i; i<max; i++){
			var textMaterial = new THREE.MeshLambertMaterial({color: Math.floor(Math.random()*16777215), emissive: Math.floor(Math.random()*16777215)});
			var percentage = ThreeD.variables.rawData[i].Frequency / ThreeD.variables.frequencyTotal;
			var geom = ThreeD.createTextGeometry(ThreeD.variables.rawData[i].Name, percentage);
			var text = new THREE.Mesh(geom, textMaterial);
			
			ThreeD.variables.names.push(text);
			//ThreeD.variables.scene.add(text);
		}
		ThreeD.layoutObjects(ThreeD.variables.names);
		ThreeD.animate();
		//ThreeD.render();
	},
	layoutObjects: function(objects){
		
		var startX = 0 - (ThreeD.variables.containerWidth  / 2),
		    startY = (ThreeD.variables.containerHeight  / 2);
				//startY = 0;
				
		var i = 0,
				max = objects.length;
		
		//initial row x layout
		var rows = [];
		rows[0] = [];
		var curXPos = startX;
		for(i; i<max; i++){
			if(curXPos + objects[i].geometry.boundingBox.max.x >= ThreeD.variables.containerWidth / 2){
				rows.push([]);
				curXPos = startX;
			}
			objects[i].position.x = curXPos;
			curXPos += objects[i].geometry.boundingBox.max.x;
			rows[rows.length - 1].push(objects[i]);
		}
		
		//y layout
		i = 0;
		max = rows.length;
		var maxHeight = 0,
		curYPos = startY;
		for(i; i < max; i++){
			var j = 0;
			var rowMax = rows[i].length;
			for(j; j<rowMax; j++){
				if(maxHeight < rows[i][j].geometry.boundingBox.max.y){
					maxHeight = rows[i][j].geometry.boundingBox.max.y;
				}
				rows[i][j].position.y = curYPos - (maxHeight / 2);
				rows[i][j].position.z = 5000;
				ThreeD.variables.scene.add(rows[i][j]);
				ThreeD.tweenToZ(rows[i][j], i);
			}
			curYPos -= maxHeight;
			maxHeight = 0;
		}
	},
	tweenToZ: function(objToTween, itemNumber){
		var curPos = {x: objToTween.position.x, y: objToTween.position.y, z: objToTween.position.z},
				target = {x: objToTween.position.x, y: objToTween.position.y, z: 0};
		var tween = new TWEEN.Tween(curPos).to(target, 1000);
		tween.onUpdate(function(){
			objToTween.position.z = curPos.z
		});
		tween.easing(TWEEN.Easing.Quintic.Out);
		tween.delay(100 * itemNumber);
		tween.start();
	},
	removeObjects: function(){
		var i = 0,
				max = ThreeD.variables.names.length;
		for(i; i<max; i++){
			ThreeD.variables.scene.remove(ThreeD.variables.names[i]);
			ThreeD.variables.names[i] = null;
		}
		ThreeD.variables.names = null;
		ThreeD.createObjects();
	},
	createTextGeometry: function(text, sizeModifier){
		var size = (2000 * sizeModifier) + 10;
		var geom = new THREE.TextGeometry(text, {size: size, height: 10, curveSegments: 6, font: "helvetiker", weight: "normal", style: "normal", material: 0, extrudeMaterial: 1});
		geom.computeBoundingBox();
		geom.computeVertexNormals();
		return geom;
	},
	animate: function(){
		requestAnimationFrame(ThreeD.animate);
		ThreeD.variables.dragControls.update();
		TWEEN.update();
		ThreeD.render();
	},
	render: function(){
		ThreeD.variables.renderer.render(ThreeD.variables.scene, ThreeD.variables.camera);
	}
};