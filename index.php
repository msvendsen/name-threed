
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Popular Names By Year</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
		<link href="css/styles.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <form method="post" id="main-form" class="form-inline navbar-form pull-right">
						<label for="year">Year:</label>
						<select name="year" id="year">
							<option value="">Please Select A Year</option>
							<?php
								$first_year = 1917;
                $last_year = 2010;

                for($first_year; $first_year <= $last_year; $first_year++){
                  echo "<option value='{$first_year}'>".$first_year."</option>";
                }
          		?>
						</select>
						<label for="gender">Gender:</label>
						<select name="gender" id="gender">
							<option value="">Please Select A Gender</option>
							<option value="female">Female</option>
							<option value="male">Male</option>
						</select>
        		<button type="submit" class="btn">GO!</button>
        	</form>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="row">
				<div class="hero-unit start-msg">
					<h1>Please Select A Year And Gender With The Form Above</h1>
				</div>
      </div>

    </div> <!-- /container -->
		<div id="three-js-container">
  		
  	</div>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/validation/jquery.validate.min.js"></script>
		<script src="js/threejs/three.min.js"></script>
		<script src="js/threejs/TrackballControls.js"></script>
		<script src="js/threejs/helvetiker_regular.typeface.js"></script>
		<script src="js/tween.min.js"></script>
		<script src="js/application.js"></script>

  </body>
</html>
